# Aays Poc
## set up the venv
```python
cd aays-poc
python -m venv venv
# for mac
source venv/bin/activate
# install requirements
pip install -r requirements.txt
# create kernel for jupyter notebook
python -m install ipykernel --user --name=aays-poc --display-name "aays-poc"
# run jupyter notebook
jupyter notebook
```
## Problem statement 1 - Time series analysis 
>  time_series_analysis.ipynb
## Problem statement 2 - Classification
> classification.ipynb
## Problem statement 3 - NLP
> NLP.ipynb 
